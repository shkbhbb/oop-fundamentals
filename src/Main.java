import java.util.Scanner;

public class Main {
    private static String[][] movies = new String[][]{
            {"The Shawshank Redemption", "9.3", "1994", "Frank Darabont"},
            {"The Godfather", "9.2", "1972", "Francis Ford Coppola"},
            {"The Dark Knight", "9.0", "2008", "Christopher Nolan"},
            {"Schindler's List", "8.9", "1993", "Steven Spielberg"},
            {"12 Angry Men", "8.9", "1957", " Sidney Lumet"},
    };


    public static void main(String[] args) {
        System.out.println("1. search in names\n2. filter release year\n3. search by director\n4. filter rates\n5. exit");

        Scanner scanner = new Scanner(System.in);

        while (true) {
            int userInput = scanner.nextInt();
            switch (userInput) {
                case 1:
                    String enteredName = scanner.next();
                    searchByName(enteredName);
                    break;
                case 2:
                    int enteredYear = scanner.nextInt();
                    filterByYear(enteredYear);
                    break;
                case 3:
                    String enteredDirector = scanner.next();
                    searchByDirector(enteredDirector);
                    break;
                case 4:
                    double enteredRate = scanner.nextDouble();
                    filterByRate(enteredRate);
                    break;
                case 5:
                    System.exit(0);
                default:
                    System.err.println("invalid input");
            }
        }
    }

    private static void filterByRate(double enteredRate) {
        for (int i = 0; i < movies.length; i++) {
            if (Double.valueOf(movies[i][3]) > enteredRate) {
                System.out.println(movies[i][0] + " " + movies[i][1] + " " + movies[i][2] + " " + movies[i][3]);
            }
        }
    }

    private static void searchByDirector(String enteredDirector) {
        for (int i = 0; i < movies.length; i++) {
            if (movies[i][2].contains(enteredDirector)) {
                System.out.println(movies[i][0] + " " + movies[i][1] + " " + movies[i][2] + " " + movies[i][3]);
            }
        }
    }

    private static void filterByYear(int enteredYear) {
        for (int i = 0; i < movies.length; i++) {
            if (Integer.parseInt(movies[i][1]) > enteredYear) {
                System.out.println(movies[i][0] + " " + movies[i][1] + " " + movies[i][2] + " " + movies[i][3]);
            }
        }
    }

    private static void searchByName(String enteredName) {
        for (int i = 0; i < movies.length; i++) {
            if (movies[i][0].contains(enteredName)) {
                System.out.println(movies[i][0] + " " + movies[i][1] + " " + movies[i][2] + " " + movies[i][3]);
            }
        }
    }
}
